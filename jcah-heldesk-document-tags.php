<?php
/*
Plugin Name: JCAH Helpdesk Document Tags
Plugin URI: http://helpdesk
Description: Grabs tags associated to documents on the category and tags pages
Author: JCAH Volunteer
Version: 1.0
Author URI: http://helpdesk/
*/

// Function that registers our widget.
add_action( 'widgets_init', 'jcah_load_widgets' ); // Tags Widget

function jcah_load_widgets() {
  register_widget( 'JCAHDocumentTags_Widget' );
}

class JCAHDocumentTags_Widget extends WP_Widget {
  
  function JCAHDocumentTags_Widget() {
    /* Widget Settings */
    $widget_ops = array( 'classname' => 'jcahdocumenttags', 'description' => 'List tags of documents associated to a category.');

    /* Widget Control Settings */
    $control_ops = array('width' => 300, 'height' => 350, 'id_base' => 'jcahdocumenttags-widget');

    /* Create the widget */
    $this->WP_Widget('jcahdocumenttags-widget', 'JCAH Document Tags', $widget_ops, $control_ops);
  }

  function widget($args, $instance) { // widget sidebar output
    
    extract($args, EXTR_SKIP);

    // Globals
    global $tags;
    global $newsCatObj;
    global $docCatObj;
    global $currentPage;
    global $hasFaqPosts;
    global $hasTrainingPosts;
    
    $assignmentFaqSlug = 'frequently-asked-questions'; // Slug string for document faq tag
    $assignmentTrainingSlug = 'training'; // Slug string for document training tag
    
    // Create simple array from a multidimensional array
    $tags = createSimpleArrayFromMultiDimensionalArray($tags);

    if(!is_home() && !is_single() && !is_page_template('page-document-revisions.php')) {

      /* User-selected settings */
      $title = apply_filters('widget_title', $instance['title']);
      
      // Defined by theme
      echo $before_widget;
      
        /* Title of widget */
        if($title) {
        
          #echo $before_title . '' . $title . '' . $after_title; // Hide for aesthetics
        
        }

        if(!empty($tags) || $hasFaqPosts == TRUE || $hasTrainingPosts == TRUE) {
        
          if(is_user_logged_in()) {
            $docTag = $_GET['doc_tag'];
          }
        
        ?>
        
        <div class="span4">
          
          <div class="well" style="padding: 8px 0; width:300px;">
      
            <ul class="nav nav-list">
              
              <?php
              
              switch ($currentPage) {
                  
                case "taxonomy_assignment":
                case "taxonomy_doc_tag":?>

                    <?php if(!empty($tags)): ?>

                      <li class="nav-header" title="Click a tag to filter">Click a tag to filter</li>

                      <?php foreach($tags as $tag): ?>
                      
                        <?php if($tag['slug'] != $assignmentFaqSlug && $tag['slug'] != $assignmentTrainingSlug):?>

                            <?php if($tag['slug'] == $docCatObj->slug): ?>
                      
                              <li class="active"><a href="?doc_tag=<?php echo $tag['slug']; ?>" title="Click for <?php echo $tag['name']; ?> documents"><i class='icon-tag icon-white'></i> <?php echo strSantizeTagStrings($tag['name']); ?></a></li>

                            <?php elseif($tag['slug'] != $docCatObj->slug): ?>

                              <li><a href="?doc_tag=<?php echo $tag['slug']; ?>" title="Click for <?php echo $tag['name']; ?> documents"><i class='icon-tag' style='opacity:0.20'></i><?php echo strSantizeTagStrings($tag['name']); ?></a></li>

                            <?php endif; ?>
                      
                        <?php endif;?>

                        <!-- Check if assignement has FAQs assigned -->
                        <?php if($tag['slug'] == $assignmentFaqSlug || $hasFaqPosts == TRUE):?>
                      
                          <?php $assignmentHasFaq = 1; ?>
                      
                        <?php endif;?>
                        <!-- End FAQ check -->

                        <!-- Check if assignment has Training material assigned -->
                        <?php if($tag['slug'] == $assignmentTrainingSlug || $hasTrainingPosts == TRUE):?>
                      
                          <?php $assignmentHasTraining = 1; ?>
                      
                        <?php endif;?>
                        <!-- End Training check -->
                      
                      <?php endforeach; ?>

                    <?php endif;?>

                    <?php if($assignmentHasFaq || $hasFaqPosts == TRUE):?>
                    
                      <li class="divider"></li>
                    
                      <li><a href="?doc_tag=<?php echo $assignmentFaqSlug; ?>" title="Click for Frequently Asked Questions"> Frequently Asked Questions</a></li>
                    
                    <?php endif;?>

                    <!-- Check for Training -->
                    <?php if($assignmentHasTraining || $hasTrainingPosts == TRUE):?>

                      <?php if($hasFaqPosts != TRUE): ?>

                        <li class="divider"></li>
                        
                      <?php endif;?>
                  
                      <li><a href="?doc_tag=training" title="Click for Training">Training</a></li>
                  
                    <?php endif;?>
                    <!-- End Training check -->

                  <?php
                    break;

                case "taxonomy_doc_tag_faq":
                case "category_faq":?>

                    <li class="nav-header" title="Click anchor for section">Click anchor for section</li>
          
                    <?php foreach($tags as $tag): ?>
          
                        <?php if($tag['slug'] != $assignmentFaqSlug && $tag['slug'] != $assignmentTrainingSlug):?>
          
                          <li><a href="#<?php echo $tag['slug']; ?>" title="Click for <?php echo $tag['name']; ?> FAQs"><i class="icon-arrow-down" style='opacity:0.20'></i><?php echo strSantizeTagStrings($tag['name']); ?></a></li>
          
                        <?php endif;?>
          
                    <?php endforeach; ?>

                  <?php
                      break;

                case "taxonomy_doc_tag_training":?>

                    <li class="nav-header" title="Click anchor for section">Click anchor for section</li>
          
                    <?php foreach($tags as $tag): ?>
          
                        <?php if($tag['slug'] != $assignmentFaqSlug && $tag['slug'] != $assignmentTrainingSlug):?>
          
                          <li><a href="#<?php echo $tag['slug']; ?>" title="Click for <?php echo $tag['name']; ?> FAQs"><i class="icon-arrow-down" style='opacity:0.20'></i><?php echo strSantizeTagStrings($tag['name']); ?></a></li>
          
                        <?php endif;?>

                        <?php if($tag['slug'] == $assignmentFaqSlug || $hasFaqPosts == TRUE):?>
                      
                          <?php $assignmentHasFaq = 1; ?>
                      
                        <?php endif;?>
          
                    <?php endforeach; ?>

                  <?php
                      break;

                case "category_news":?>

                    <li class="nav-header" title="Click a tag to filter">Click a tag to filter</li>
                      
                      <?php foreach($tags as $tag): ?>
                      
                        <?php if($tag['slug'] != 'faq' && $tag['slug'] != 'news'):?>
                      
                            <?php if($tag['slug'] == $newsCatObj->slug): ?>
                            
                              <li class="active"><a href="?tag=<?php echo $tag['slug']; ?>" title="Click for news about <?php echo $tag['name']; ?>"><i class='icon-tag icon-white'></i><?php echo strSantizeTagStrings($tag['name']); ?></a></li>
                            
                            <?php elseif($tag['slug'] != $newsCatObj->slug):?>

                              <li><a href="?tag=<?php echo $tag['slug']; ?>" title="Click for news about <?php echo $tag['name']; ?>"><i class='icon-tag' style='opacity:0.20'></i><?php echo strSantizeTagStrings($tag['name']); ?></a></li>

                            <?php endif; ?>
                      
                        <?php endif;?>
                      
                      <?php endforeach; ?>

                  <?php
                      break;

                case "tag_template":?>

                    <li class="nav-header" title="Click a tag to filter">Click a tag to filter</li>
                      
                      <?php foreach($tags as $tag): ?>
                      
                        <?php if($tag['slug'] != 'faq' && $tag['slug'] != 'news'):?>
                      
                            <?php if($tag['slug'] == $newsCatObj->slug): ?>
                            
                              <li class="active"><a href="?tag=<?php echo $tag['slug']; ?>" title="Click for news about <?php echo $tag['name']; ?>"><i class='icon-tag icon-white'></i><?php echo strSantizeTagStrings($tag['name']); ?></a></li>
                            
                            <?php elseif($tag['slug'] != $newsCatObj->slug):?>

                              <li><a href="?tag=<?php echo $tag['slug']; ?>" title="Click for news about <?php echo $tag['name']; ?>"><i class='icon-tag' style='opacity:0.20'></i><?php echo strSantizeTagStrings($tag['name']); ?></a></li>

                            <?php endif; ?>
                      
                        <?php endif;?>
                      
                      <?php endforeach; ?>

                  <?php
                      break;
              }
              ?>

            </ul>

          </div>

        </div>

        <?php

          echo $after_widget; // post-widget code from theme
      }else{
        echo '<div class="span12">
                <div class="alert alert-block">
                  <h4 class="alert-heading">Alert</h4>
                  <p>No tags assigned to documents</p>
                </div>
              </div>';
      }
    }
  }

  function update($new_instance, $old_instance) {
    $instance = $old_instance;

    /* Strip tags (if needed) and update the widget settings. */
    $instance['title'] = strip_tags( $new_instance['title'] );

    return $instance;
  }

  function form($instance) {
    /* Set up some default widget settings. */
    $defaults = array( 'title' => 'Search Documents' );
    $instance = wp_parse_args( (array) $instance, $defaults ); ?>

      <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title
        <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
        </label>
      </p>
    
    <?php
  }
}
?>